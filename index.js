// Import thư viện express.js vào. Dạng Import express from "express";
const express = require("express");

//Import thư viện mongoosejs
const mongoose = require("mongoose");

// Khởi tạo app express
const app = express();

// Khai báo sẵn 1 port trên hệ thống
const port = 8000;

// Import router Module
const courseRouter = require("./app/routes/courseRouter");
const reviewRouter = require("./app/routes/reviewRouter");


app.use((req, res, next) => {
    let today = new Date();

    console.log("Current: ", today);

    next();
})

app.use((req, res, next) => {
    console.log("Method: ", req.method);

    next();
})

mongoose.connect("mongodb://localhost:27017/CRUD_course",(error) => {
    if (error) throw error;

    console.log("Connect successfully!")
});

// Khai báo API
app.get("/", (req, res) => {
    let today = new Date();

    // Response trả 1 chuỗi JSON có status 200
    res.status(200).json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

app.use(courseRouter);
app.use(reviewRouter);

// Callback function
// + Nó là 1 function
// + Nó là 1 tham số của 1 hàm khác
// + Nó sẽ chạy sau khi hàm chủ thể được thực hiện
// Lắng nghe app trên cổng được khai báo 8000
app.listen(port, () => {
    console.log("App listening on port: ", port);
});

